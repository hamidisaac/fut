<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FIFA 2019 - Trade Pile</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<h1>Trade Pile</h1>
<div class="flex-center position-ref full-height">
    <table border="1">
        <thead>
            <tr>
                <th>#</th>
                <th>tradeId</th>
                <th>tradeState</th>
                <th>buyNowPrice</th>
                <th>currentBid</th>
                <th>bidState</th>
                <th>startingBid</th>
                <th>expires</th>
                <th>sellerName</th>
                <th>itemType</th>
                <th>name</th>
                <th>Sell</th>
            </tr>
        </thead>
        <tbody>
        @php
        $i = 1;
        @endphp
        @foreach($items['auctionInfo'] as $item)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $item['tradeIdStr'] }}</td>
                <td>{{ $item['tradeState'] }}</td>
                <td>{{ $item['buyNowPrice'] }}</td>
                <td>{{ $item['currentBid'] }}</td>
                <td>{{ $item['bidState'] }}</td>
                <td>{{ $item['startingBid'] }}</td>
                <td>{{ $item['expires'] }}</td>
                <td>{{ $item['sellerName'] }}</td>
                <td>{{ $item['itemData']['itemType'] }}</td>
                @if(isset($item['itemData']['name']))
                    <td>{{ $item['itemData']['name'] }}</td>
                @else
                    <td></td>
                @endif
                <td><a href="{{$baseUrl}}/sell?id={{$item['itemData']['id']}}&bid={{$item['startingBid']}}&buy_now={{$item['buyNowPrice']}}">Sell</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
</body>
</html>
