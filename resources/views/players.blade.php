<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FIFA 2019 - Players</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<h1>Players</h1>
<div class="flex-center position-ref full-height">
    <table border="1">
        <thead>
            <tr>
                <th>#</th>
                <th>tradeId</th>
                <th>tradeState</th>
                <th>buyNowPrice</th>
                <th>currentBid</th>
                <th>bidState</th>
                <th>startingBid</th>
                <th>expires</th>
                <th>sellerName</th>
                <th>Bid (startingBid + 50)</th>
            </tr>
        </thead>
        <tbody>
        @php
            $i = 1;
        @endphp
        @foreach($items as $item)
            @foreach ($item as $player)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $player['tradeIdStr'] }}</td>
                    <td>{{ $player['tradeState'] }}</td>
                    <td>{{ $player['buyNowPrice'] }}</td>
                    <td>{{ $player['currentBid'] }}</td>
                    <td>{{ $player['bidState'] }}</td>
                    <td>{{ $player['startingBid'] }}</td>
                    <td>{{ $player['expires'] }}</td>
                    <td>{{ $player['sellerName'] }}</td>
                    <td><a href="{{$baseUrl}}/bid?tradeId={{$player['tradeIdStr']}}&bid={{$player['startingBid']+50}}">Bid ({{$player['startingBid']+50}})</a></td>
                </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>

</div>
</body>
</html>
