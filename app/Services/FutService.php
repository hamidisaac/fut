<?php
/**
 * Created by PhpStorm.
 * User: isaac
 * Date: 8/22/19
 * Time: 2:46 AM
 */

namespace App\Services;

use FUTApi\Core;
use FUTApi\FutError;

class FutService
{
    private $loginData;
    private $fut;

    public function __construct($email, $password, $platform, $code)
    {
        $this->loginData = [
            'email' => $email,
            'password' => $password,
            'platform' => $platform,
            'backup_code' => $code,
        ];
        info('create core', $this->loginData);
    }

    public function login()
    {
        try {
            info("Logging into " . $this->loginData['email']);
            $this->fut = new Core(
                $this->loginData['email'],
                $this->loginData['password'],
                strtolower($this->loginData['platform']),
                $this->loginData['backup_code'],
                false,
                false,
                storage_path(
                    'app/ea_cookies/'.md5($this->loginData['email'])
                )
            );
            $data = $this->fut->login();
            return $data;
        }catch (FutError $exception) {
            $error = $exception->GetOptions();
            info("Error in Logging in ".$error['reason']." on account: ".$this->loginData['email']);
            return null;
        }
    }

    public function searchPlayers()
    {
        try {
            $search = $this->fut->searchAuctions('player');
            return $search;
        }catch (FutError $exception) {
            $error = $exception->GetOptions();
            info("Error in Searching Players ".$error['reason']." on account: ".$this->loginData['email']);
            return null;
        }
    }

    public function bid($tradeId, $bid)
    {
        try {
            $res = $this->fut->bid($tradeId, $bid);
            return $res;
        }catch (FutError $exception) {
            $error = $exception->GetOptions();
            info("Error in Biding ".$error['reason']." on account: ".$this->loginData['email']);
            return null;
        }
    }

    public function tradepile()
    {
        try {
            $search = $this->fut->tradepile();
            return $search;
        }catch (FutError $exception) {
            $error = $exception->GetOptions();
            info("Error in Trade Pile ".$error['reason']." on account: ".$this->loginData['email']);
            return null;
        }
    }

    public function sell($id, $bid, $buy_now)
    {
        try {
            $res = $this->fut->sell($id, $bid, $buy_now);
            return $res;
        }catch (FutError $exception) {
            $error = $exception->GetOptions();
            info("Error in Selling ".$error['reason']." on account: ".$this->loginData['email']);
            return null;
        }
    }

    public function logout()
    {
        try {
            $this->fut->logout();
            return 'Logout Successfully';
        }catch (FutError $exception) {
            $error = $exception->GetOptions();
            info("Error in Logging Out ".$error['reason']." on account: ".$this->loginData['email']);
            return null;
        }
    }
}