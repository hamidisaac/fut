<?php

namespace App\Http\Controllers;

use App\Helpers\Google2FA;
use App\Services\FutService;
use Illuminate\Http\Request;
use Session;

class FutController extends Controller
{
    protected $baseUrl;
    protected $email;
    protected $password;
    protected $platform;
    protected $secret;
    protected $otp;
    protected $isLoggedIn = false;
    protected $msgLoginSuccess    = "Login Successfully";
    protected $msgLoginUnSuccess  = "We have an error logging in";

    public function __construct()
    {
        $this->baseUrl      = url('/');
        $this->email        = 'fmpicixf@myeasymailbox.com';
        $this->password     = 'Ad791235789';
        $this->platform     = 'ps4';
        $this->secret       = 'ju6c7wha3l4jsvt3';
        $this->otp          = Google2FA::generateOneTimeCode($this->secret);
    }

    public function login()
    {
        $this->futService = new FutService($this->email, $this->password, $this->platform, $this->otp);
        $loginData = $this->futService->login();
        if (isset($loginData['auth']['access_token'])) {
            $msg                = $this->msgLoginSuccess;
            $this->isLoggedIn   = true;
        } else {
            $msg                = $this->msgLoginUnSuccess;
            $this->isLoggedIn   = false;
        }
        return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => $msg]);
    }

    public function players()
    {
        $this->futService = new FutService($this->email, $this->password, $this->platform, $this->otp);
        $loginData = $this->futService->login();
        if (isset($loginData['auth']['access_token'])) {
            $this->isLoggedIn   = true;
            $items = $this->futService->searchPlayers();
            if ($items)
                return view('players', ['baseUrl' => $this->baseUrl, 'items' => $items]);
            return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => "Error in Searching Players"]);
        } else {
            return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => $this->msgLoginUnSuccess]);
        }
    }

    public function bid(Request $request)
    {
        $this->futService = new FutService($this->email, $this->password, $this->platform, $this->otp);
        $loginData = $this->futService->login();
        if (isset($loginData['auth']['access_token'])) {
            $this->isLoggedIn = true;
            $res = $this->futService->bid($request->get('tradeId'), $request->get('bid'));
            if ($res)
                return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => "Bid Operation Completed"]);
            return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => "Error in Biding"]);
        } else {
            return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => $this->msgLoginUnSuccess]);
        }
    }

    public function tradePile()
    {
        $this->futService = new FutService($this->email, $this->password, $this->platform, $this->otp);
        $loginData = $this->futService->login();
        if (isset($loginData['auth']['access_token'])) {
            $this->isLoggedIn = true;
            $items = $this->futService->tradepile();
            if($items)
                return view('tradepile', ['baseUrl' => $this->baseUrl, 'items' => $items]);
            return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => "Error in Trade Pile"]);
        } else {
            return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => $this->msgLoginUnSuccess]);
        }
    }

    public function sell(Request $request)
    {
        $this->futService = new FutService($this->email, $this->password, $this->platform, $this->otp);
        $loginData = $this->futService->login();
        if (isset($loginData['auth']['access_token'])) {
            $this->isLoggedIn = true;
            $res = $this->futService->sell($request->get('id'), $request->get('bid'), $request->get('buy_now'));
            if($res)
                return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => "Sell Operation Completed"]);
            return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => "Error in Selling"]);
        } else {
            return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => $this->msgLoginUnSuccess]);
        }
    }

    public function logout()
    {
        $this->futService = new FutService($this->email, $this->password, $this->platform, $this->otp);
        $loginData = $this->futService->login();
        if (isset($loginData['auth']['access_token'])) {
            $this->isLoggedIn = true;
            $res = $this->futService->logout();
            if($res) {
                $this->isLoggedIn = false;
                return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => "Logout Successfully"]);
            }
            return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => "Error in Logging Out"]);
        } else {
            return view('welcome', ['baseUrl' => $this->baseUrl, 'isLoggedIn' => $this->isLoggedIn, 'email' => $this->email, 'msg' => $this->msgLoginUnSuccess]);
        }
    }
}
