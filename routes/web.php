<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FutController@login');
Route::get('/players', 'FutController@players');
Route::get('/bid', 'FutController@bid');
Route::get('/tradepile', 'FutController@tradePile');
Route::get('/sell', 'FutController@sell');
Route::get('/logout', 'FutController@logout');
